"""
Neovim remote plugin which reads configuration from Pycodestyle.

Pycodestyle is a plugin which lets us define style linter settings in a text
file. Multiple files can be defined (one global, one per project) and
Pycodestyle will merge their settings appropriately.

This plugin lets us access these final settings programmatically from inside
Neovim.
"""

import os
import pynvim  # type: ignore
from configparser import ConfigParser
from pathlib import Path
from typing import Iterator, List
from optparse import OptionParser

PROJECT_FILES = ['setup.cfg', 'tox.ini']


def split(s: str) -> List[str]:
    """Split a string on commas. Returns an empty list for the empty string."""
    return s.split(',') if s else []


TYPE_CONVERTERS = {
    'verbose': int,
    'quiet': int,
    'repeat': bool,
    'exclude': split,
    'select': split,
    'ignore': split,
    'show-source': bool,
    'show-pep8': bool,
    'statistics': bool,
    'count': bool,
    'max-line-length': int,
    'max-doc-length': int,
    'indent-size': int,
    'hang-closing': bool,
    'diff': bool,
    'benchmark': bool
}


def parents(current: Path) -> Iterator[Path]:
    """Generator which moves upwards the directory hierarchy of a path."""
    if current.is_file():
        current = current.parent
    while True:
        yield current
        if current.root == str(current):
            break
        current = current.parent


@pynvim.plugin
class NvimPycodestyle(object):

    def __init__(self, vim):
        self.vim = vim
        try:
            from pycodestyle import get_parser  # type: ignore
            parser: OptionParser = get_parser()
            self.defaults = parser.get_default_values().__dict__
            # Normalize names to match their config file option
            for key in list(self.defaults.keys()):
                new_key = key.replace('_', '-')
                if new_key != key and new_key in parser.config_options:
                    self.defaults[new_key] = self.defaults[key]
                    del self.defaults[key]
        except ModuleNotFoundError as e:
            if e.name != 'pycodestyle':
                raise e
            self.defaults = {}

    @pynvim.function('PycodestyleSettings', sync=True)
    def get_settings(self, args):
        config = ConfigParser(allow_no_value=True)
        config.read_dict({'pycodestyle': self.defaults})

        # User configuration file
        if os.name == 'nt':
            config.read(Path.home() / '.pycodestyle')
        else:
            conf_dir = os.getenv('XDG_CONFIG_HOME', Path.home() / '.config')
            config.read(Path(conf_dir) / 'pycodestyle')

        # for-else is a trick to break out of the enclosing loop
        for path in parents(Path(os.getcwd())):
            for file in PROJECT_FILES:
                if config.read(path / file):
                    break
            else:
                continue
            break

        result = dict(config['pycodestyle'])

        # All the values are strings, convert them to their actual types
        for key, converter in TYPE_CONVERTERS.items():
            if key in result and result[key] is not None:
                result[key] = converter(result[key])

        return result
