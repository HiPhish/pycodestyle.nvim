.. default-role:: code


###########
 Changelog
###########


1.0.1 - 2021-09-03
##################

Added
=====
- pycodestyle is now optional


1.0.0 - 2021-09-01
##################

Initial commit
