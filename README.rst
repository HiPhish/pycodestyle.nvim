.. default-role:: code

##################
 pycodestyle.nvim
##################

This plugin can fetch the final computed settings for pycodestyle_ as a
dictionary. Why would you want that? You could add something like the following
to your `ftplugin/python.vim` file:

.. code-block:: vim

   let pycodestyle_config = PycodestyleGetConfig()
   let &textwidth = get(pycodestyle_config, 'max-line-length', &textwidth)
   let &tabstop = get(pycodestyle_config, 'indent-size', &tabstop)

With this configuration your text width setting for Python files will always
match your linter settings or fall back to your personal default. If your
project has linter settings anyway why not use those instead of duplicating
them in your editor configuration? And since pycodestyle can be configured per
project the above code example will pick up the project-specific settings or
fall back on your global pycodestyle configuration. In the absence of any
configuration the result will fall back on pycodestyle's own defaults, whatever
they may be.


Installation
############

This is a remote plugin written in Python and using pycodestyle as a library.
You will therefore need Python 3, the pynvim_ plugin host and of course
pycodestyle (which you probably already have if want to use this).

.. default-role:: sh

   pip3 install pynvim pycodestyle

Then install this plugin like any other Neovim plugin and execute
`:UpdateRemotePlugins` in order to generate the plugin manifest. See
`:h :UpdateRemotePlugins` for more details.

.. note::
   There is a bug in pynvim 0.4.3 where a function will return `v:null` the
   first time it is called. You can call the function a second time to be
   certain.


License
#######

Released under the terms of the MIT (Expat) license, see the COPYING_ file for
details.


.. _pycodestyle: https://pycodestyle.pycqa.org/en/latest/
.. _pynvim: https://github.com/neovim/pynvim
.. _COPYING: COPYING.txt
